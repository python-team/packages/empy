#!/bin/sh
# autopkgtest check: Build and run a program against glib, to verify that the
# headers and pkg-config file are installed correctly
# (C) 2024 Jose Luis Rivero
# Author: Jose Luis Rivero <jrivero@osrfoundation.org>

set -ex
trap "rm -rf $AUTOPKGTEST_TMP" 0 INT QUIT ABRT PIPE TERM
DEBIAN_DIR=$(pwd)/debian
cp ${DEBIAN_DIR}/sample.* ${AUTOPKGTEST_TMP}/
cd ${AUTOPKGTEST_TMP}
if em.py sample.em | diff -u sample.bench -; then
  echo "empy call ok"
else
  exit 1
fi
